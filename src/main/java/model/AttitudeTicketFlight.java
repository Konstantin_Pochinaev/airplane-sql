package model;

/**
 * @author Pochinaev Kostya
 */
public class AttitudeTicketFlight {

    private String ticketNumber;
    private int flightId;
    private String fareConditions;
    private double amount;

    public AttitudeTicketFlight(String ticketNumber, int flightId, String fareConditions, double amount) {
        this.ticketNumber = ticketNumber;
        this.flightId = flightId;
        this.fareConditions = fareConditions;
        this.amount = amount;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getFareConditions() {
        return fareConditions;
    }

    public void setFareConditions(String fareConditions) {
        this.fareConditions = fareConditions;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
