package model;

import org.postgresql.geometric.PGpoint;

/**
 * @author Pochinaev Kostya
 */
public class Airport {

    private String code;
    private String name;
    private String city;
    private String coordinates;
    private PGpoint exactCoordinates;
    private String timezone;

    public Airport(String code, String name, String city, PGpoint coordinates, String timezone) {
        this.code = code;
        this.name = name;
        this.city = city;
        this.coordinates = parseCoordinates(coordinates);
        this.timezone = timezone;
        exactCoordinates = coordinates;
    }

    public String parseCoordinates(PGpoint coordinates) {
        return String.format("X: %.6f, Y: %.6f", coordinates.x, coordinates.y);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public PGpoint getExactCoordinates() {
        return exactCoordinates;
    }

    public void setExactCoordinates(PGpoint exactCoordinates) {
        this.exactCoordinates = exactCoordinates;
    }
}
