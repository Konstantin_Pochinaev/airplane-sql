package model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Pochinaev Kostya
 */
public class Booking {

    private String bookingNumber;
    private LocalDateTime bookingDate;
    private double totalAmount;
    private String formatBookingDate;

    public Booking(String bookingNumber, Timestamp bookingDate, double totalAmount) {
        this.bookingNumber = bookingNumber;
        this.bookingDate = bookingDate.toLocalDateTime();
        this.totalAmount = totalAmount;
        formatBookingDate = this.bookingDate.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
    }


    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public LocalDateTime getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDateTime bookingDate) {
        this.bookingDate = bookingDate;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getFormatBookingDate() {
        return formatBookingDate;
    }

    public void setFormatBookingDate(String formatBookingDate) {
        this.formatBookingDate = formatBookingDate;
    }
}
