package model;

import org.json.JSONObject;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class Ticket {

    private String ticketNumber;
    private String bookingNumber;
    private String passengerId;
    private String passengerName;
    private JSONObject contactData;
    private String formatContactData;

    public Ticket(String ticketNumber, String bookingNumber, String passengerId, String passengerName, String contactData) {
        this.ticketNumber = ticketNumber;
        this.bookingNumber = bookingNumber;
        this.passengerId = passengerId;
        this.passengerName = passengerName;
        this.contactData = new JSONObject(contactData);
        parseContactData();
    }

    private void parseContactData() {
        if (contactData.length() == 2) {
            formatContactData = "Phone: " + contactData.getString("phone") +
                    ", Email: " + contactData.getString("email");
        } else {
            formatContactData = "Phone: " + contactData.getString("phone");
        }
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public JSONObject getContactData() {
        return contactData;
    }

    public void setContactData(JSONObject contactData) {
        this.contactData = contactData;
    }

    public String getFormatContactData() {
        return formatContactData;
    }

    public void setFormatContactData(String formatContactData) {
        this.formatContactData = formatContactData;
    }
}
