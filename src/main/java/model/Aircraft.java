package model;

/**
 * @author Pochinaev Kostya
 */
public class Aircraft {

    private String code;
    private String model;
    private int range;

    public Aircraft(String code, String model, int range) {
        this.code = code;
        this.model = model;
        this.range = range;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }
}
