package model;

/**
 * @author Pochinaev Kostya
 */
public class BoardingPasses {

    private String ticketNumber;
    private int flightId;
    private int boardingNumber;
    private String seatNumber;

    public BoardingPasses(String ticketNumber, int flightId, int boardingNumber, String seatNumber) {
        this.ticketNumber = ticketNumber;
        this.flightId = flightId;
        this.boardingNumber = boardingNumber;
        this.seatNumber = seatNumber;
    }


    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getBoardingNumber() {
        return boardingNumber;
    }

    public void setBoardingNumber(int boardingNumber) {
        this.boardingNumber = boardingNumber;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }
}
