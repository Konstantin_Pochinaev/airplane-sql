package model;

/**
 * @author Pochinaev Kostya
 */
public class Seat {

    private String aircraftCode;
    private String seatNumber;
    private String fareConditions;

    public Seat(String aircraftCode, String seatNumber, String fareConditions) {
        this.aircraftCode = aircraftCode;
        this.seatNumber = seatNumber;
        this.fareConditions = fareConditions;
    }


    public String getAircraftCode() {
        return aircraftCode;
    }

    public void setAircraftCode(String aircraftCode) {
        this.aircraftCode = aircraftCode;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getFareConditions() {
        return fareConditions;
    }

    public void setFareConditions(String fareConditions) {
        this.fareConditions = fareConditions;
    }
}
