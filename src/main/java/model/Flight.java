package model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Pochinaev Kostya
 */
public class Flight {

    private int flightId;
    private String flightNumber;
    private LocalDateTime scheduledDeparture;
    private String formatScheduledDeparture;
    private LocalDateTime scheduledArrival;
    private String formatScheduledArrival;
    private String departureAirport;
    private String arrivalAirport;
    private String status;
    private String aircraftCode;
    private LocalDateTime actualDeparture;
    private String formatActualDeparture;
    private LocalDateTime actualArrival;
    private String formatActualArrival;

    public Flight(int flightId, String flightNumber, Timestamp scheduledDeparture, Timestamp scheduledArrival,
                  String departureAirport, String arrivalAirport, String status, String aircraftCode,
                  Timestamp actualDeparture, Timestamp actualArrival) {
        this.flightId = flightId;
        this.flightNumber = flightNumber;
        this.scheduledDeparture = scheduledDeparture.toLocalDateTime();
        this.scheduledArrival = scheduledArrival.toLocalDateTime();
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.status = status;
        this.aircraftCode = aircraftCode;
        if (actualDeparture == null) {
            this.actualDeparture = null;
            formatActualDeparture = "null";
        } else {
            this.actualDeparture = actualDeparture.toLocalDateTime();
            formatActualDeparture = this.actualDeparture.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
        }
        if (actualArrival == null) {
            this.actualArrival = null;
            formatActualArrival = "null";
        } else {
            this.actualArrival = actualArrival.toLocalDateTime();
            formatActualArrival = this.actualArrival.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
        }
        formatScheduledArrival = this.scheduledArrival.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
        formatScheduledDeparture = this.scheduledDeparture.format(DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss"));
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFormatScheduledDeparture() {
        return formatScheduledDeparture;
    }

    public void setFormatScheduledDeparture(String formatScheduledDeparture) {
        this.formatScheduledDeparture = formatScheduledDeparture;
    }

    public LocalDateTime getScheduledDeparture() {
        return scheduledDeparture;
    }

    public void setScheduledDeparture(LocalDateTime scheduledDeparture) {
        this.scheduledDeparture = scheduledDeparture;
    }

    public LocalDateTime getScheduledArrival() {
        return scheduledArrival;
    }

    public void setScheduledArrival(LocalDateTime scheduledArrival) {
        this.scheduledArrival = scheduledArrival;
    }

    public String getFormatScheduledArrival() {
        return formatScheduledArrival;
    }

    public void setFormatScheduledArrival(String formatScheduledArrival) {
        this.formatScheduledArrival = formatScheduledArrival;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAircraftCode() {
        return aircraftCode;
    }

    public void setAircraftCode(String aircraftCode) {
        this.aircraftCode = aircraftCode;
    }

    public LocalDateTime getActualDeparture() {
        return actualDeparture;
    }

    public void setActualDeparture(LocalDateTime actualDeparture) {
        this.actualDeparture = actualDeparture;
    }

    public String getFormatActualDeparture() {
        return formatActualDeparture;
    }

    public void setFormatActualDeparture(String formatActualDeparture) {
        this.formatActualDeparture = formatActualDeparture;
    }

    public LocalDateTime getActualArrival() {
        return actualArrival;
    }

    public void setActualArrival(LocalDateTime actualArrival) {
        this.actualArrival = actualArrival;
    }

    public String getFormatActualArrival() {
        return formatActualArrival;
    }

    public void setFormatActualArrival(String formatActualArrival) {
        this.formatActualArrival = formatActualArrival;
    }
}
