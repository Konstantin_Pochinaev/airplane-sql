package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Booking;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class BookingRepository {

    public static ObservableList<Booking> getBookingList() {
        ObservableList<Booking> bookings = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM bookings");
            while (set.next()) {
                bookings.add(new Booking(set.getString(1),
                        set.getTimestamp(2), set.getDouble(3)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return bookings;
    }
}
