package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Flight;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class FlightRepository {

    public static ObservableList<Flight> getFlightList() {
        ObservableList<Flight> flights = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM flights");
            while (set.next()) {

                flights.add(new Flight(set.getInt(1), set.getString(2),
                        set.getTimestamp(3), set.getTimestamp(4), set.getString(5),
                        set.getString(6), set.getString(7), set.getString(8),
                        set.getTimestamp(9), set.getTimestamp(10)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return flights;
    }
}
