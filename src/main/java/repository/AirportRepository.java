package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Airport;
import org.postgresql.geometric.PGpoint;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class AirportRepository {

    public static ObservableList<Airport> getAirportList() {
        ObservableList<Airport> airports = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM airports");
            while (set.next()) {
                airports.add(new Airport(set.getString(1), set.getString(2),
                        set.getString(3), (PGpoint) set.getObject(4), set.getString(5)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return airports;
    }
}
