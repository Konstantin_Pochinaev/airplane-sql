package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.AttitudeTicketFlight;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class AttitudeTicketFlightRepository {

    public static ObservableList<AttitudeTicketFlight> getAttitudeTicketFlightList() {
        ObservableList<AttitudeTicketFlight> attitudeTicketFlights = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM ticket_flights");
            while (set.next()) {
                attitudeTicketFlights.add(new AttitudeTicketFlight(set.getString(1), set.getInt(2),
                        set.getString(3), set.getDouble(4)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return attitudeTicketFlights;
    }

}
