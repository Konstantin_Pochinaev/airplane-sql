package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Aircraft;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class AircraftRepository {

    public static ObservableList<Aircraft> getAircraftList() {
        ObservableList<Aircraft> aircraft = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM aircrafts");
            while (set.next()) {
                aircraft.add(new Aircraft(set.getString(1),
                        set.getString(2), set.getInt(3)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return aircraft;
    }
}
