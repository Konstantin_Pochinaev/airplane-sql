package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Seat;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class SeatRepository {

    public static ObservableList<Seat> getSeatList() {
        ObservableList<Seat> seats = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM seats");
            while (set.next()) {
                seats.add(new Seat(set.getString(1),
                        set.getString(2), set.getString(3)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return seats;
    }
}
