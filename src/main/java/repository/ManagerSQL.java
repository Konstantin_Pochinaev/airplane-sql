package repository;

import javafx.application.Platform;

import java.sql.*;

/**
 * @author Pochinaev Kostya
 */
public class ManagerSQL {

    private final Connection connection;
    private static ManagerSQL managerSQL;

    private ManagerSQL() {
        connection = getConnection();
    }

    public static ManagerSQL getManager() {
        if (managerSQL == null) {
            managerSQL = new ManagerSQL();
        }
        return managerSQL;
    }

    public Statement getStatement() throws SQLException {
        return connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
    }

    private static Connection getConnection() {
        try {
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/demo",
                    "postgres", "k100984601");
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public void closeConnection() {
        Platform.exit();
        new Thread(() -> {
            try {
                Thread.sleep(30000);
                connection.close();
            } catch (InterruptedException | SQLException e) {
                e.printStackTrace();
            }
        });
    }
}
