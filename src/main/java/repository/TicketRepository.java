package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Ticket;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class TicketRepository {

    public static ObservableList<Ticket> getTicketList() {
        ObservableList<Ticket> tickets = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM tickets");
            while (set.next()) {
                tickets.add(new Ticket(set.getString(1), set.getString(2),
                        set.getString(3), set.getString(4), set.getString(5)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return tickets;
    }

}
