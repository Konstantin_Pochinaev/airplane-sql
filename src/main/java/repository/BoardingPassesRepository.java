package repository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.BoardingPasses;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class BoardingPassesRepository {

    public static ObservableList<BoardingPasses> getPassesList() {
        ObservableList<BoardingPasses> passes = FXCollections.observableArrayList();
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery("SELECT * FROM boarding_passes");
            while (set.next()) {
                passes.add(new BoardingPasses(set.getString(1),
                        set.getInt(2), set.getInt(3), set.getString(4)));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return passes;
    }
}
