package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import repository.ManagerSQL;
import view.tab.QueryTab;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class SQLConsole {

    public TextArea sqlQueryTextArea;
    public Label errorLabel;
    private QueryTab queryTab;

    @FXML
    public void initialize() {
        queryTab = (QueryTab) QueryTab.getInstance();
    }

    @FXML
    public void execSQLCommand() {
        clearGridPane();
        errorLabel.setText("");
        String queryText = sqlQueryTextArea.getText();
        queryText = queryText.toUpperCase();
        String[] queries = queryText.split(";");
        execSelectCommand(queries[0]);
    }

    private void execSelectCommand(String query) {
        try {
            ResultSet set = ManagerSQL.getManager().getStatement().executeQuery(query);
            ResultSetMetaData metaData = set.getMetaData();
            int columnCount = metaData.getColumnCount();
            set.last();
            int rowsCount = set.getRow();
            set.first();
            queryTab.setResult(columnCount, rowsCount, set, metaData);
        } catch (SQLException exception) {
            errorLabel.setText(exception.getMessage());
        }
    }

    @FXML
    public void nextPage() {
        try {
            queryTab.nextPage();
        } catch (SQLException exception) {
            errorLabel.setText(exception.getMessage());
        }
    }

    @FXML
    public void clearGridPane() {
        queryTab.clearInfo();
    }

    @FXML
    public void showTablesInfo() {
        execSelectCommand("SELECT table_name, column_name, data_type" +
                " FROM information_schema.columns WHERE table_schema='bookings' ORDER BY table_name;");
    }
}
