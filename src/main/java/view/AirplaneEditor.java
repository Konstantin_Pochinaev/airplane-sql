package view;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import view.tab.*;

import java.io.IOException;

/**
 * @author Pochinaev Kostya
 */
public class AirplaneEditor {

    public VBox rootVBox;
    public TabPane tabPane;
    public ProgressBar progressBar;
    private AbstractTab[] tabs;
    private int fontSize;
    private static final int FONT_BONUS = 6;
    private Stage sqlConsole;

    @FXML
    private void initialize() throws IOException {
        fontSize = 13;
        rootVBox.setStyle("-fx-font-size: " + fontSize);
        sqlConsole = new Stage();
        sqlConsole.setScene(new Scene(FXMLLoader.load(getClass().getResource("/fxml/SQLConsole.fxml"))));
        sqlConsole.setTitle("SQL Console");
        sqlConsole.getIcons().add(new Image("/img/console.png"));
        sqlConsole.setMinWidth(280);
        sqlConsole.setMinHeight(200);
        createTabList();
        updateData();
    }

    @FXML
    public void notifyGarbageCollector() {
        Runtime.getRuntime().gc();
    }

    @FXML
    public void increaseFontSize() {
        if (fontSize < 20) {
            rootVBox.setStyle("-fx-font-size: " + ++fontSize);
            for (AbstractTab tab : tabs) {
                tab.changeImageSize(fontSize + FONT_BONUS);
            }
        }
    }

    @FXML
    public void reduceFontSize() {
        if (fontSize > 9) {
            rootVBox.setStyle("-fx-font-size: " + --fontSize);
            for (AbstractTab tab : tabs) {
                tab.changeImageSize(fontSize + FONT_BONUS);
            }
        }
    }

    private void createTabList() {
        tabs = new AbstractTab[]{AircraftTab.getInstance(), AirportTab.getInstance(),
                PassesTab.getInstance(), BookingTab.getInstance(), FlightTab.getInstance(), SeatTab.getInstance(),
                TicketTab.getInstance(), AttitudeTicketFlightTab.getInstance()};
    }

    @FXML
    public void openSQLConsole() {
        sqlConsole.show();
        tabPane.getSelectionModel().selectLast();
    }

    @FXML
    public void updateData() {
        Task<Object> task = new Task<>() {
            @Override
            protected Object call() throws InterruptedException {
                for (int i = 0; i < tabs.length; i++) {
                    tabs[i].select();
                    updateProgress(i + 1, tabs.length);
                }
                Thread.sleep(100);
                progressBar.setVisible(false);
                return null;
            }
        };
        if (!progressBar.isVisible()) {
            progressBar.setVisible(true);
            progressBar.progressProperty().unbind();
            progressBar.progressProperty().bind(task.progressProperty());
            new Thread(task).start();
        }
    }
}
