package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import model.Airport;
import repository.ManagerSQL;

import java.sql.SQLException;
import java.sql.Statement;

import static repository.AirportRepository.*;

/**
 * @author Pochinaev Kostya
 */
public class AirportTab extends AbstractTab {

    public Tab airportTab;
    public TableView<Airport> airportTable;
    public TableColumn<Airport, String> airportCodeTableColumn;
    public TableColumn<Airport, String> airportNameTableColumn;
    public TableColumn<Airport, String> cityTableColumn;
    public TableColumn<Airport, String> coordinatesTableColumn;
    public TableColumn<Airport, String> timezoneTableColumn;
    private static AbstractTab instance;

    @FXML
    private void initialize() {
        instance = this;
        imageView = new ImageView("/img/airport.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        airportTab.setGraphic(imageView);
        airportCodeTableColumn.setCellValueFactory(new PropertyValueFactory<>("code"));
        airportNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        cityTableColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
        coordinatesTableColumn.setCellValueFactory(new PropertyValueFactory<>("coordinates"));
        timezoneTableColumn.setCellValueFactory(new PropertyValueFactory<>("timezone"));
    }

    @Override
    public void select() {
        airportTable.setItems(getAirportList());
    }

    public static AbstractTab getInstance() {
        return instance;
    }

}
