package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import model.Seat;

import static repository.SeatRepository.*;

/**
 * @author Pochinaev Kostya
 */
public class SeatTab extends AbstractTab {

    public Tab seatTab;
    public TableView<Seat> seatTable;
    public TableColumn<Seat, String> aircraftCodeTableColumn;
    public TableColumn<Seat, String> seatNumberTableColumn;
    public TableColumn<Seat, String> fareConditionsTableColumn;
    private static AbstractTab instance;

    @FXML
    public void initialize() {
        instance = this;
        imageView = new ImageView("/img/seat.png");
        imageView.setFitWidth(19);
        imageView.setFitHeight(19);
        seatTab.setGraphic(imageView);

        aircraftCodeTableColumn.setCellValueFactory(new PropertyValueFactory<>("aircraftCode"));
        seatNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("seatNumber"));
        fareConditionsTableColumn.setCellValueFactory(new PropertyValueFactory<>("fareConditions"));
    }

    @Override
    public void select() {
        seatTable.setItems(getSeatList());
    }

    public static AbstractTab getInstance() {
        return instance;
    }
}
