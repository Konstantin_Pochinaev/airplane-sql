package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import model.Booking;

import java.util.Date;

import static repository.BookingRepository.*;


/**
 * @author Pochinaev Kostya
 */
public class BookingTab extends AbstractTab {

    public Tab bookingTab;
    public TableView<Booking> bookingTable;
    public TableColumn<Booking, String> bookingRefTableColumn;
    public TableColumn<Booking, Date> bookingDateTableColumn;
    public TableColumn<Booking, Double> totalAmountTableColumn;
    private static AbstractTab instance;

    @FXML
    public void initialize() {
        instance = this;
        imageView = new ImageView("/img/booking.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        bookingTab.setGraphic(imageView);

        bookingRefTableColumn.setCellValueFactory(new PropertyValueFactory<>("bookingNumber"));
        bookingDateTableColumn.setCellValueFactory(new PropertyValueFactory<>("formatBookingDate"));
        totalAmountTableColumn.setCellValueFactory(new PropertyValueFactory<>("totalAmount"));
    }


    public static AbstractTab getInstance() {
        return instance;
    }

    @Override
    public void select() {
        bookingTable.setItems(getBookingList());
    }
}
