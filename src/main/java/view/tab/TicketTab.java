package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import model.Ticket;

import static repository.TicketRepository.*;

/**
 * @author Pochinaev Kostya
 */
public class TicketTab extends AbstractTab {

    public Tab ticketTab;
    public TableView<Ticket> ticketTable;
    public TableColumn<Ticket, String> ticketNumberTableColumn;
    public TableColumn<Ticket, String> bookingNumberTableColumn;
    public TableColumn<Ticket, String> passengerIdTableColumn;
    public TableColumn<Ticket, String> passengerNameTableColumn;
    public TableColumn<Ticket, String> contactDataColumn;
    private static AbstractTab instance;

    @FXML
    private void initialize() {
        instance = this;
        imageView = new ImageView("/img/ticket.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        ticketTab.setGraphic(imageView);
        ticketNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("ticketNumber"));
        bookingNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("bookingNumber"));
        passengerIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("passengerId"));
        passengerNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("passengerName"));
        contactDataColumn.setCellValueFactory(new PropertyValueFactory<>("formatContactData"));
    }

    @Override
    public void select() {
        ticketTable.setItems(getTicketList());
    }

    public static AbstractTab getInstance() {
        return instance;
    }

}
