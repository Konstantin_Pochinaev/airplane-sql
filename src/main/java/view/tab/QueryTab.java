package view.tab;

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * @author Pochinaev Kostya
 */
public class QueryTab extends AbstractTab {

    private static AbstractTab instance;
    public Tab queryTab;
    public GridPane gridPane;
    public int rowIndex, columnCount, rowCount;
    private ResultSet resultSet;
    private ResultSetMetaData metaData;
    private static final int META_DATA_ROW_COUNT = 1;

    @FXML
    public void initialize() {
        instance = this;
        imageView = new ImageView("/img/SQL.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        queryTab.setGraphic(imageView);
    }

    public void insertRow(TextField field, int columnIndex, int rowIndex) {
        if (rowIndex == 0) {
            field.getStyleClass().add("metaDataLabel");
        }
        field.setEditable(false);
        field.setMinHeight(35D);
        field.setAlignment(Pos.CENTER);
        GridPane.setValignment(field, VPos.CENTER);
        GridPane.setHalignment(field, HPos.CENTER);
        GridPane.setHgrow(field, Priority.ALWAYS);
        GridPane.setVgrow(field, Priority.ALWAYS);
        gridPane.add(field, columnIndex, rowIndex);
    }

    public void setResult(int columnCount, int rowCount, ResultSet resultSet, ResultSetMetaData metaData)
            throws SQLException {
        rowIndex = 1;
        this.resultSet = resultSet;
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        this.metaData = metaData;
        nextPage();
    }

    public void nextPage() throws SQLException {
        if (resultSet != null) {
            if (rowIndex == 1) {
                for (int i = 1; i <= columnCount; i++) {
                    insertRow(new TextField(metaData.getColumnName(i)), i, 0);
                }
            }
            for (int i = 0; i < 400; i++) {
                if (rowIndex == rowCount + META_DATA_ROW_COUNT) {
                    break;
                }
                for (int j = 1; j <= columnCount; j++) {
                    insertRow(new TextField(resultSet.getString(j)), j, rowIndex);
                }
                rowIndex++;
                resultSet.next();
            }
        }
    }

    public void clearInfo() {
        gridPane.setGridLinesVisible(false);
        gridPane.getColumnConstraints().clear();
        gridPane.getRowConstraints().clear();
        gridPane.getChildren().clear();
        gridPane.setGridLinesVisible(true);
    }

    public static AbstractTab getInstance() {
        return instance;
    }

    @Override
    public void select() {
        clearInfo();
    }
}
