package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import model.BoardingPasses;

import static repository.BoardingPassesRepository.*;

/**
 * @author Pochinaev Kostya
 */
public class PassesTab extends AbstractTab {

    public Tab passesTab;
    public TableView<BoardingPasses> passesTable;
    public TableColumn<BoardingPasses, String> ticketNumberTableColumn;
    public TableColumn<BoardingPasses, Integer> flightIdTableColumn;
    public TableColumn<BoardingPasses, Integer> boardingNumberTableColumn;
    public TableColumn<BoardingPasses, String> seatNumberTableColumn;
    private static AbstractTab instance;

    @FXML
    private void initialize() {
        instance = this;
        imageView = new ImageView("/img/passes.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        passesTab.setGraphic(imageView);

        ticketNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("ticketNumber"));
        flightIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("flightId"));
        boardingNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("boardingNumber"));
        seatNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("seatNumber"));
    }

    public void select() {
        passesTable.setItems(getPassesList());
    }

    public static AbstractTab getInstance() {
        return instance;
    }
}
