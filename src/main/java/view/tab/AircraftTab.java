package view.tab;

import com.sun.javafx.scene.control.IntegerField;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.util.converter.IntegerStringConverter;
import model.Aircraft;
import repository.ManagerSQL;
import view.AirplaneEditor;

import java.sql.SQLException;
import java.sql.Statement;

import static repository.AircraftRepository.getAircraftList;

/**
 * @author Pochinaev Kostya
 */
public class AircraftTab extends AbstractTab {

    public Tab aircraftTab;
    public TableColumn<Aircraft, String> codeTableColumn;
    public TableColumn<Aircraft, String> modelTableColumn;
    public TableColumn<Aircraft, Integer> rangeTableColumn;
    public TableView<Aircraft> aircraftTable;
    private static AbstractTab instance;

    @FXML
    public void initialize() {
        instance = this;
        imageView = new ImageView("/img/icon.png");
        imageView.setFitWidth(19);
        imageView.setFitHeight(19);
        aircraftTab.setGraphic(imageView);

        codeTableColumn.setCellValueFactory(new PropertyValueFactory<>("code"));
        modelTableColumn.setCellValueFactory(new PropertyValueFactory<>("model"));
        rangeTableColumn.setCellValueFactory(new PropertyValueFactory<>("range"));

        setEditCommit();
    }

    @Override
    public void select() {
        aircraftTable.setItems(getAircraftList());
    }

    public static AbstractTab getInstance() {
        return instance;
    }

    private void setEditCommit() {
        modelTableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        modelTableColumn.setOnEditCommit(event -> {
            try {
                Statement statement = ManagerSQL.getManager().getStatement();
                int updateCount = statement.executeUpdate("UPDATE aircrafts_data SET model=model || '{\"ru\":\"" +
                        event.getNewValue() + "\"}' WHERE aircraft_code='" + event.getRowValue().getCode() + "'");
                if (updateCount > 0) {
                    event.getRowValue().setModel(event.getNewValue());
                }
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        });

        rangeTableColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        rangeTableColumn.setOnEditCommit(event -> {
            try {
                Statement statement = ManagerSQL.getManager().getStatement();
                int updateCount = statement.executeUpdate("UPDATE aircrafts_data SET range=" + event.getNewValue()
                        + " WHERE range=" + event.getOldValue());
                if (updateCount > 0) {
                    event.getRowValue().setRange(event.getNewValue());
                }
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        });
    }

}
