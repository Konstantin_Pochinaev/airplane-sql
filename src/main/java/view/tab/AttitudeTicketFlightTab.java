package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import model.AttitudeTicketFlight;

import static repository.AttitudeTicketFlightRepository.*;

/**
 * @author Pochinaev Kostya
 */
public class AttitudeTicketFlightTab extends AbstractTab {

    public Tab attitudeTab;
    public TableView<AttitudeTicketFlight> attitudeTable;
    public TableColumn<AttitudeTicketFlight, String> ticketNumberTableColumn;
    public TableColumn<AttitudeTicketFlight, Integer> flightIdTableColumn;
    public TableColumn<AttitudeTicketFlight, String> amountTableColumn;
    public TableColumn<AttitudeTicketFlight, Double> fareConditionsTableColumn;
    private static AbstractTab instance;

    @FXML
    private void initialize() {
        instance = this;
        imageView = new ImageView("/img/attitude.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        attitudeTab.setGraphic(imageView);
        ticketNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("ticketNumber"));
        flightIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("flightId"));
        amountTableColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        fareConditionsTableColumn.setCellValueFactory(new PropertyValueFactory<>("fareConditions"));
    }

    @Override
    public void select() {
        attitudeTable.setItems(getAttitudeTicketFlightList());
    }

    public static AbstractTab getInstance() {
        return instance;
    }

}
