package view.tab;

import javafx.scene.image.ImageView;

/**
 * @author Pochinaev Kostya
 */
public abstract class AbstractTab {

    protected ImageView imageView;

    public void changeImageSize(int size) {
        imageView.setFitWidth(size);
        imageView.setFitHeight(size);
    }

    public abstract void select();
}
