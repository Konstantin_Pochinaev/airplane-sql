package view.tab;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import model.Flight;

import static repository.FlightRepository.*;

/**
 * @author Pochinaev Kostya
 */
public class FlightTab extends AbstractTab {

    public Tab flightTab;
    public TableView<Flight> flightTable;
    public TableColumn<Flight, Integer> flightIdTableColumn;
    public TableColumn<Flight, String> flightNumberTableColumn;
    public TableColumn<Flight, String> scheduledDepartureTableColumn;
    public TableColumn<Flight, String> scheduledArrivalTableColumn;
    public TableColumn<Flight, String> departureAirportTableColumn;
    public TableColumn<Flight, String> arrivalAirportTableColumn;
    public TableColumn<Flight, String> statusTableColumn;
    public TableColumn<Flight, String> aircraftCodeTableColumn;
    public TableColumn<Flight, String> actualDepartureTableColumn;
    public TableColumn<Flight, String> actualArrivalTableColumn;
    private static AbstractTab instance;

    @FXML
    private void initialize() {
        instance = this;
        imageView = new ImageView("/img/flight.png");
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        flightTab.setGraphic(imageView);

        flightIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("flightId"));
        flightNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("flightNumber"));
        scheduledDepartureTableColumn.setCellValueFactory(new PropertyValueFactory<>("formatScheduledDeparture"));
        scheduledArrivalTableColumn.setCellValueFactory(new PropertyValueFactory<>("formatScheduledArrival"));
        departureAirportTableColumn.setCellValueFactory(new PropertyValueFactory<>("departureAirport"));
        arrivalAirportTableColumn.setCellValueFactory(new PropertyValueFactory<>("arrivalAirport"));
        statusTableColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        aircraftCodeTableColumn.setCellValueFactory(new PropertyValueFactory<>("aircraftCode"));
        actualDepartureTableColumn.setCellValueFactory(new PropertyValueFactory<>("formatActualDeparture"));
        actualArrivalTableColumn.setCellValueFactory(new PropertyValueFactory<>("formatActualArrival"));
    }

    public static AbstractTab getInstance() {
        return instance;
    }

    @Override
    public void select() {
        flightTable.setItems(getFlightList());
    }
}
