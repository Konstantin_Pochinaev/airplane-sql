package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import repository.ManagerSQL;

import java.io.IOException;

/**
 * @author Pochinaev Kostya
 */

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Airplane Editor");
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(400);
        primaryStage.getIcons().add(new Image("/img/icon.png"));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AirplaneEditor.fxml"));
        primaryStage.setScene(new Scene(loader.load()));
        loader.getController();
        primaryStage.setOnCloseRequest(event -> ManagerSQL.getManager().closeConnection());
        primaryStage.show();
    }
}
